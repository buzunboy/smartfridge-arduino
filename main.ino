// Imported Classes
#include "HX711.h"
#include <dht.h>
#include <SoftwareSerial.h>

// Defining pins for Therm. Sensor
#define dht_dpin A0

// Defining pins for Weight 1 Sensor
#define DOUT1  A1
#define CLK1  2

// Defining pins for Weight 2 Sensor
#define DOUT2  A2
#define CLK2  3

// Defining pins for Weight 3 Sensor
#define DOUT3  A3
#define CLK3  11

// Defining pins for Weight 4 Sensor
#define DOUT4  A4
#define CLK4  12

// Defining pins for Color Sensor
#define S0 4
#define S1 5
#define S2 6
#define S3 7
#define sensorOut 8

HX711 scale1(DOUT1, CLK1);
HX711 scale2(DOUT2, CLK2);
HX711 scale3(DOUT3, CLK3);
HX711 scale4(DOUT4, CLK4);

dht DHT;
SoftwareSerial mySerial(10, 9); //RX,TX

float calibration_factor = 2125; 
int frequency = 0;
int red = 0;
int blue = 0;
int green = 0;

void setup() {
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);
  
  digitalWrite(S0,HIGH);
  digitalWrite(S1,LOW);
  
  pinMode(sensorOut, INPUT);
  
  Serial.begin(9600);
  mySerial.begin(9600);
  scale1.set_scale(calibration_factor);
  scale1.tare(); 
  scale2.set_scale(calibration_factor);
  scale2.tare(); 
  scale3.set_scale(calibration_factor);
  scale3.tare(); 
  scale4.set_scale(calibration_factor);
  scale4.tare(); 
  
  long zero_factor1 = scale1.read_average();
  Serial.print("Zero factor1: "); 
  Serial.println(zero_factor1);
  long zero_factor2 = scale2.read_average();
  Serial.print("Zero factor2: "); 
  Serial.println(zero_factor2);
  long zero_factor3 = scale3.read_average();
  Serial.print("Zero factor3: "); 
  Serial.println(zero_factor3);
  long zero_factor4 = scale4.read_average();
  Serial.print("Zero factor4: "); 
  Serial.println(zero_factor4);
}

void loop() {
    readColor();
    //getData();
    //createJSONString();
    sendData();
    delay(2000);
}

void readColor() {
  digitalWrite(S2,LOW);
  digitalWrite(S3,LOW);
  frequency = pulseIn(sensorOut, LOW);
  red = frequency;
  delay(100);
  digitalWrite(S2,HIGH);
  digitalWrite(S3,HIGH);
  frequency = pulseIn(sensorOut, LOW);
  green = frequency;
  delay(100);
  digitalWrite(S2,LOW);
  digitalWrite(S3,HIGH);
  frequency = pulseIn(sensorOut, LOW);
  blue = frequency;

}

void getData() {
// Check if a message has been received
    String msg = getMessage();
    if(msg!=""){
      Serial.println(msg);
    }
 
    // Send the text you entered in the input field of the Serial Monitor to the HC-06
    if(Serial.available()){
      mySerial.write(Serial.read());
    }
}

String getMessage(){
  String msg = "";
  char a;
  
  while(mySerial.available()) {
      a = mySerial.read();
      msg+=String(a);
  }
  return msg;
}


float getWeight1() {
  return abs(scale1.get_units()) + abs(scale2.get_units());
}

float getWeight2() {
  return abs(scale3.get_units()) + abs(scale4.get_units());
}

int getColor() {
  if (red <= green) {
    if (red <= blue) {
      return 0;
    } else {
      return 2;
    }
  } else if (red <= blue) {
    if (red <= green) {
      return 0;
    } else {
      return 1;
    }
  } else {
    if (blue <= green) {
      return 2;
    } else {
      return 1;
    }
  }
}

double getTemperature() {
  DHT.read11(dht_dpin);
   return DHT.temperature;
}

double getHumidity() {
  DHT.read11(dht_dpin);
  return DHT.humidity;
}

void sendData() {
/* mySerial.print("'/Shelves/shelf0', data={'currentWeight':");
 mySerial.print(getWeight(), 1);
 mySerial.print("}, params={'print': 'pretty'}");
 mySerial.println(); */
 mySerial.println("111");
 mySerial.println(getWeight1(), 3);
 mySerial.println("112");
 mySerial.println(getWeight2(), 3);
 mySerial.println("222");
 mySerial.println(getTemperature());
 mySerial.println("333");
 mySerial.println(getHumidity());
}

